var spans = document.getElementsByTagName('span');
var btn_start = document.getElementById('start');
var btn_stop = document.getElementById('stop');
var h=0, mn=0,s=0,ms=0;
var timer ;
console.log(spans);
function start(){
    timer = setInterval(update_chrono,100)
    btn_start.disabled = true;
}

function update_chrono(){
    console.log('update_chrono');
    ms+=1;
    if(ms==10){
        ms=1;
        s+=1
    }

    if(s==60){
        s=0;
        mn+=1
    }
    if(mn==60){
        mn=0;
        h+=1
    }
    
    spans[0].innerHTML = h + " h";
    spans[1].innerHTML = mn + " min";
    spans[2].innerHTML = s + " s";
    spans[3].innerHTML = ms + " ms";
}

function stop(){
    clearInterval(timer);
    btn_start.disabled = false;
}

function reset(){
h=0, mn=0,s=0,ms=0;
spans[0].innerHTML = h + " h";
spans[1].innerHTML = mn + " min";
spans[2].innerHTML = s + " s";
spans[3].innerHTML = ms + " ms";
}